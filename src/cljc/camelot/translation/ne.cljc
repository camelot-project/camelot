(ns camelot.translation.ne)

(def t-ne
  {:words
   {:acknowledge "स्विकार्नु "
    :add ": थम्नुहोस "
    :advanced "उच्च "
    :and-lc "र "
    :back "पछाडी "
    :of-lc "को "
    :on-lc "मा "
    :or-lc "मा "
    :cancel "रद्द गर्नु "
    :continue "जारी राख्नुहोस "
    :citation "उद्धरन "
    :close "बन्द गर्नुहोस "
    :confirm "निश्चित गर्नुहोस "
    :create "सिर्जना गर्नुहोस "
    :dataset "डाटासेट "
    :next "अर्को "
    :date "मिति "
    :delete "मेटौनुहोस "
    :details "विवरण "
    :done "सकियो "
    :edit "सम्पादन गर्नुहोस "
    :hide "लुकाउनुहोस् "
    :present "उपस्थित "
    :history "इतिहास "
    :import "आयात गर्नुहोस "
    :in "भित्र "
    :name "नाम "
    :no "होइन "
    :notes "टिप्पणीहरु "
    :or "अथवा "
    :to-lc "लाई "
    :at-lc "मा "
    :remove "हटाउनुहोस "
    :revert "उल्टौनुहोस "
    :search "खोजि गर्नुहोस "
    :select "चयन गर्नुहोस "
    :submit "बुझौनुहोस "
    :update "अपडेट गर्नुहोस "
    :uploading "अपलोड गर्दै "
    :version "संस्करण "
    :yes "हो"}

:problems
   {:root-path-missing "कृपया सर्वेक्षण निर्देशिकाको सम्पूर्ण मार्ग  सेटिंग्स प्यानलमा निर्दिष्ट गर्नुहोस्"
    :root-path-not-found "सर्वेक्षणको निर्देशिका भेटियन अथवा पढ्न सकिएन "
    :path-not-found "%s: मार्ग भेटिएन "
    :not-directory "%s: निर्देशिका होइन "
    :species-quantity-missing "%s: मात्रा बिनाको प्रजाति छ "
    :species-name-missing "%s: प्रजाति बिनाको मात्रा छ "
    :config-not-found ": कन्फिगरेसन फाइल भेटिएन। कृपया 'init' विकल्पकोसाथ camelot चलाउनुहोस अनि बनाएको कन्फिगरेसन फाइल समायोजित गर्नुहोस"}

  :survey
   {:duplicate-name "'%s ' नामक सर्वेक्षण पहिलेनै छ "
    :title "सर्वेक्षण "
    :report-description "रिपोर्ट गर्नुकोलागी सर्वेक्षण"
    :sidebar-title "सर्वेक्षणहरु "
    :survey-name.label "सर्वेक्षणको नाम "
    :survey-name.description "सर्वेक्षण उल्लेख गर्नकोलागी प्रयोग गरिने नाम "
    :survey-directory.label "सर्वेक्षण निर्देशिका "
    :survey-directory.description "सर्वेक्षणको डेटा को मूल निर्देशिका"
    :survey-sampling-point-density.label "नमुना पोइन्ट घनत्व (मीटर)"
    :survey-sampling-point-density.description "सर्वेक्षण परिका क्यामेरा जाल स्टेशनहरु बीचको दुरी "
    :survey-sighting-independence-threshold.label "साईटिंग स्वतनत्रता थ्रेसोल्ड (मिनेट) "
    :survey-sighting-independence-threshold.description "नया साईटिंग स्वतनत्र हुनकालागि चाहिने न्यूनतम कुलजम्मा मिनेट जुन सुरुको साईटिंग र तेस्पचिको साईटिंग को बीच बितेको हुनपर्छ  "
    :survey-notes.label "सर्वेक्षणको वर्णन "
    :survey-notes.description "यो सर्वेक्षणको लक्ष्यहरू र  विधिहरुको विवरण"}

:survey-site
   {:title "सर्वेक्षण स्थल "
    :report-description "एउटा सर्वेक्षण भित्रको डेटा "
    :sidebar-title "सर्वेक्षण स्थलहरु "
    :site-id.label "स्थल "
    :site-id.description "यो सर्वेक्षणमा हाल्ने स्थल "}

   :trap-station-session-camera
   {:title "सेसन क्यामेरा "
  :sidebar-title  "सेसन क्यामेराहरू "
    :trap-station-session-camera-import-path.label "आयात मार्ग "
    :trap-station-session-camera-import-path.description "यस क्यामेरामा मूल रूपमा आयात गरिएको फाईलहरूको मार्ग"
    :trap-station-session-camera-media-unrecoverable.label "अप्रापनीय माध्यम"
    :trap-station-session-camera-media-unrecoverable.description "यो क्यामेरा सँग सम्बन्धित माध्यम पुनःप्राप्ति गर्न सकिएन "
    :camera-id.label "क्यामेरा "
    :camera-id.description "यो जाल स्टेशनह शत्रमा थप्नुपर्ने क्यामेरा "
    :delete-media  "माध्यम मेटौनुहोस"}

   :trap-station
   {:title "जाल स्टेशन"
    :sidebar-title "जाल स्टेशनहरु "
    :report-description "रिपोर्ट गर्नुपर्ने जाल स्टेशन "
    :trap-station-name.label "जाल स्टेशनको नाम "
    :trap-station-name.description "यो जाल स्टेशनको नाम"
    :trap-station-longitude.label "देशान्तर"
    :trap-station-longitude.description" दशमलव ढाँचामा  जीपीएस देशान्तर"
    :trap-station-latitude.label "अक्षांश"
    :trap-station-latitude.description "दशमलव ढाँचामा  GPS अक्षांश"
    :trap-station-altitude.label "उचाई (मिटर)"
    :trap-station-altitude.description  "समुद्री सतहको सापेक्षमा मिटरमा उचाई"
    :trap-station-distance-above-ground.label "जमिन माथिको उचाई (मीटर)"
    :trap-station-distance-above-ground.description "क्यामेरा जालो जमिनमाथि राखिएको दुरी "
    :trap-station-distance-to-road.label "सडक सम्मको दुरी (मीटर)"
    :trap-station-distance-to-road.description "क्यामेरा जालो र सबैभन्दा नजिकको सडक बीचको दुरी"
    :trap-station-distance-to-river.label "खोला सम्मको दुरी (मीटर)"
    :trap-station-distance-to-river.description "क्यामेरा जालो र सबैभन्दा नजिकको खोला बीचको दुरी"
    :trap-station-distance-to-settlement.label "बस्ती सम्मको दुरी (मीटर)"
    :trap-station-distance-to-settlement.description "क्यामेरा जालो र सबैभन्दा नजिकको बस्ती बीचको दुरी"
    :trap-station-sublocation.label "उपस्थान "
    :trap-station-sublocation.description "स्थानलाई दिइएको नाम "
    :trap-station-notes.label "जाल स्टेशन टिप्पणीहरु "
    :trap-station-notes.description "यो जाल स्टेशनबारे टिप्पणीहरु "}
   :trap-station-session
   {:title "जाल स्टेशन शत्र"
    :sidebar-title   "शत्रहरु"
    :trap-station-session-start-date.label   "शत्र सुरुहुने मिति"
    :trap-station-session-start-date.description   "यो शत्रकोलागि जाल स्टेशनले रेकर्ड गर्न सुरु गरेको मिति"
    :trap-station-session-end-date.label   "शत्र समाप्तहुने  मिति"
    :trap-station-session-end-date.description   "यो शत्रकोलागि जाल स्टेशनले रेकर्ड गर्न समाप्त गरेको मिति"
    :trap-station-session-notes.label   "शत्र टिप्पणीहरु"
    :trap-station-session-notes.description  "यो क्यामेरा स्टेशनको लागी शत्र सम्बन्धि टिप्पणीहरु"}

   :site
   {:duplicate-name "'%s ' को स्थल पहिले देखि अवस्थित छ"
    :title  "स्थल"
    :sidebar-title  "स्थलहरु"
    :site-name.label  "स्थलको नाम"
    :site-name.description  "यो स्थलको सन्दर्भमा प्रयोग हुने नाम"
    :site-country.label  "देश"
    :site-country.description  "त्यो देश जुन  भित्र यो स्थल रहन्छ"
    :site-state-province.label  "राज्य/प्रान्त"
    :site-state-province.description  "त्यो राज्य वा प्रान्त जुन  भित्र यो स्थल रहन्छ"
    :site-city.label  "सहर"
    :site-city.description  "त्यो सहर वा सबैभन्दा नजिकको सहर जुन भित्र यो स्थल रहन्छ"
    :site-sublocation.label  "उपस्थान"
    :site-sublocation.description  "यो स्थलले प्रतिनिधित्व गर्ने स्थानको नाम"
    :site-area.label  "स्थलको क्ष्यत्रफल (km2)"
    :site-area.description  "यस स्थलको क्ष्यत्रफल कि.मि2"
    :site-notes.label  "स्थल टिप्पणीहरु"
    :site-notes.description  "यस स्थल सम्बन्धित टिप्पणीहरु"}

   :camera
    {:duplicate-name "'%s ' को क्यामेरा पहिले देखि अवस्थित छ"
    :title    "क्यामेरा"
    :sidebar-title    "क्यामेराहरु "
    :camera-name.label   "क्यामेको  नाम "
    :camera-name.description   "त्यो नाम जसले यस क्यामेरालाई चिन्निने छ "
    :camera-status-id.label   "स्थिति"
    :camera-status-id.description   "यो क्यामेरा कि सक्रिय छ , प्रयोग गर्नको लागि उपलब्ध छ , वा केहि कारणले रिटायर भएको छ"
    :camera-make.label   "बनावट"
    :camera-make.description   "यो क्यामेराको निर्माता वा ब्राण्ड"
    :camera-model.label   "नमुना (मोडेल)"
    :camera-model.description   "यो क्यामेरा को नमुनाको (मोडेल) नाम वा संख्या"
    :camera-software-version.label   "क्यामेराको सफ्टवेयर संस्करण"
    :camera-software-version.description   "यस क्यामेरा मा चलिरेहेको सफ्टवेयरको संस्करण"
    :camera-notes.label   "विवरणहरु"
     :camera-notes.description   "यस क्यामेरा सम्बन्धि विवरणहरु"}

    :sighting
   {:sidebar-title "साईटिंगहरु "
    :title  "साईटिंग"
    :taxonomy-id.label  "प्रजातिहरु "
    :taxonomy-id.description  "माध्यममा भएको प्रजातिहरु"
    :sighting-quantity.label  "संख्या"
    :sighting-quantity.description  "माध्यममा यो प्रजातिको संख्या"}

   :media
   {:sidebar-title "माध्यम"
    :title  "माध्यम"
    :media-filename.label  "फाइलको नाम"
    :media-filename.description  "तस्बिर"
    :media-capture-timestamp.label  "क्याप्चर भएको समय "
    :media-capture-timestamp.description  "क्याप्चर भएको समय "
    :media-notes.label  "टिप्पणीहरु "
    :media-notes.description  "यस तस्बिर सम्बन्धित  टिप्पणीहरु"}

   :photo
   {:sidebar-title "तस्बिर"
    :title  "तस्बिरको  विवरण"
    :photo-iso-setting.label  "ISO सेटिंग्स"
    :photo-iso-setting.description  "यो तस्बिर खिचिएको समयको ISO सेटिंग्स"
    :photo-aperture-setting.label  "Aperture"
    :photo-aperture-setting.description  "तस्बिर खिचिएको समयमा लेन्सको aperture"
    :photo-exposure-value.label  "exposure को  मान"
    :photo-exposure-value.description  "यस तस्बिरको  गणना गरिएको exposure को  मान"
    :photo-flash-setting.label  "Flashको  सेटिंग"
    :photo-flash-setting.description  "तस्बिर खिचिएको समयमा flashको  सेटिंग"
    :photo-fnumber-setting.label  "F - मात्रा "
    :photo-fnumber-setting.description  "तस्बिर खिचिएको समयमा F - मात्राको  सेटिंग"
    :photo-orientation.label  "अभिमुखीकरण"
    :photo-orientation.description  "तस्बिर खिचिएको समयमा क्यामेराको अभिमुखीकरण"
    :photo-resolution-x.label  "चौडाई (पिक्सेल)"
    :photo-resolution-x.description  "तस्बिरको  चौडाई पिक्सेलमा "
    :photo-resolution-y.label  "उचाई (पिक्सेल)"
    :photo-resolution-y.description  "तस्बिरको उचाई पिक्सेलमा"
    :photo-focal-length.label  "फोकल लम्भई"
    :photo-focal-length.description  "तस्बिर खिचिएको समयमा फोकल लम्भई"}

   :taxonomy
   {:sidebar-title "प्रजाति"
    :title  "प्रजाति"
    :report-description  "विवरण दिनुपर्ने प्रजाति"
    :taxonomy-class.label  "वर्ग"
    :taxonomy-class.description  "वर्गको नाम"
    :taxonomy-order.label  "अर्डर"
    :taxonomy-order.description  "अर्डरको नाम "
    :taxonomy-family.label  "परिवार"
    :taxonomy-family.description  "परिवारको नाम"
    :taxonomy-genus.label  "जीनस"
    :taxonomy-genus.description  "जीनसको नाम"
    :taxonomy-species.label  "प्रजाति"
    :taxonomy-species.description  "प्रजातिको नाम"
    :taxonomy-common-name.label  "साधारण नाम"
    :taxonomy-common-name.description  "साधारण नाम जुन बाट यस वर्गीकरणलाइ चिनिन्छ"
    :species-mass-id.label  "प्रजातिको तौल"
    :species-mass-id.description  "औसत वयस्कको लगभग तौल"
    :taxonomy-notes.label  "टिप्पणीहरु"
    :taxonomy-notes.description  "यस प्रजाति वा यसको परिचय सम्बन्धित टिप्पणीहरु"}

   :camera-status
   {:active "क्ष्यत्रमा सक्रिय"
    :available  "प्रयोग गर्नको लागि उपलब्ध छ"
    :lost  "हराएको"
    :stolen  "चोरिएको"
    :retired  "सेवानिवृत्त"}

   :checks
   {:starting "निरन्तरताको लागि जाँचदै। ..."
    :failure-notice  "असफल %s"
    :problem-without-reason  "%s. ठ्याक्कै कारण परिचित छैन"
    :stddev-before  "'%s' मा भएको समयछाप अरु समयछाप भन्दा धेरै अगाडी छ"
    :stddev-after  "'%s' मा भएको समयछाप अरु समयछाप भन्दा धेरै  छ"
    :project-date-before  "'%s' मा भएको समयछाप प्रोजेक्ट सुरुवात मिति भन्दा अगाडी छ"
    :project-date-after  "'%s' मा भएको समयछाप प्रोजेक्ट सुरुवात मिति भन्दा पछाडी छ"
    :time-light-sanity  "तस्बिरको exposure मानले देखौछ कि यस समयछाप धेरै तस्विरहरुमा गलत हुनसक्छ"
    :camera-checks  "तस्बिरहरुमा दुइ वा बढी तस्बिरहरु क्यामेरा चेकपोइन्टहरु भनेर संकेत गरिएको छैन"
    :headline-consistency  "तस्विरहरुको हेडलाईनहरु तस्बिरहरुपार फरक छन्  ('%s' र  '%s' परिक्षण गरिएको)"
    :source-consistency  "तस्विरहरुको चरण तस्बिरहरुपार फरक छन्  ('%s' र  '%s' परिक्षण गरिएको)"
    :camera-consistency  "तस्विर खिच्नलाई  प्रयोग भएका क्यामेरा फरक छन्  ('%s' र  '%s' परिक्षण गरिएको)"
    :required-fields  "चाहिने क्षेत्रहरु '%s'  '%s' भेटिएनन"
    :album-has-data  "अवधिको लागि डेटासेट खाली हुन हुदैन"
    :sighting-consistency-quantity-needed  "'%s' मा  प्रजाति साइटिंग भेटियो, तर मात्रा भेटियेन"
    :sighting-consistency-species-needed  "'%s' मा मात्रा साइटिंग भेटियो, तर  प्रजाति भेटियेन"
    :surveyed-species  "'%s' मा  प्रजाति पहिचान गरियो जुन सर्वेक्षण '%s' मा छैनन्"
    :future-timestamp  "'%s' मा भएको समयछाप भविष्यमा छ"
    :invalid-photos  "फाइलमा एक वा बढी क्षेत्रहरू'%s' छैनन्"
    :inconsistent-timeshift  "मूल मिति/समय बाटको समय बदलाव यस फोल्डरमा साविक मुताबिक छैन ('%s' र  '%s' परिक्षण गरिएको)"
    :gps-data-missing  "'%s' GPS डाटा छैन. अहिलेको लागि, माध्यम GPS देशान्तर र  अक्षांश सेट नगरिकन  आयात गर्न मिल्दैन "}

   :language
   {:en "अंग्रेजी "
    :vn "भित्नमिस"
    :ne "नेपाली"}

   :metadatametadata
   {:location.gps-longitude "GPS देशान्तर"
    :location.gps-longitude-ref  "GPS देशान्तरसन्दर्भ"
    :location.gps-latitude  "GPS अक्षांश"
    :location.gps-latitude-ref  "GPS अक्षांशसन्दर्भ"
    :location.gps-altitude  "GPS उचाई"
    :location.gps-altitude-ref  "GPS उचाई सन्दर्भ"
    :location.sublocation  "उप -स्थान"
    :location.city  "सहर"
    :location.state-province  "राज्य / प्रान्त"
    :location.country  "देश"
    :location.country-code  "देश को कोड"
    :location.map-datum  "GPS नक्सा को मिति"
    :camera-settings.aperture  "एपर्चर सेटिंग"
    :camera-settings.exposure  "exposure सेटिंग"
    :camera-settings.flash  "फ्ल्यास सेटिंग"
    :camera-settings.focal-length  "फोकल लम्बाई सेटिंग"
    :camera-settings.fstop  "एफ-स्टप सेटिंग"
    :camera-settings.iso  "ISO सेटिंग"
    :camera-settings.orientation  "अभिमुखीकरण सेटिंग"
    :camera-settings.resolution-x  "X- रिजोलुसन"
    :camera-settings.resolution-y  "Y - रिजोलुसन"
    :camera.make  "क्यामेरा बनाउने"
    :camera.model  "क्यामेरा नमुना"
    :camera.software  "क्यामेरा सफ्टवेयर"
    :datetime  "मिति / समय"
    :headline  "मुख्यसमाचार"
    :artist  "कलाकार"
    :phase  "चरण"
    :copyright  "प्रतिलिपि अधिकार"
    :description  "वर्णन"
    :filename  "फाइलको नाम"
    :filesize  "फाइल साइज"}

   :settings
   {:preferences  "प्राथमिकताहरू"
    :survey-settings  "सेटिंग"
    :title  "सेटिंग"
    :erroneous-infrared-threshold.label  "गलत अवरक्त संघार"
    :erroneous-infrared-threshold.description  "मिति/समय त्रुटि पत्ता लगाउनको   लागि    0. 0 र 1.0 बीचको मान, संघारको रुपमा तोकिन्छ "
    :infrared-iso-value-threshold.label  "अवरक्त ISO मान संघार"
    :infrared-iso-value-threshold.description  "तस्बीरको ISO मान भन्दा पर जस्लाई रात सोचिन्छ"
    :sighting-independence-minutes-threshold.label  "साइटिंग स्वतन्त्रता संघार (मिनेट)"
    :sighting-independence-minutes-threshold.description  "नयाँ साइट स्वतन्त्र मनिनको लागि प्रारम्भिक साइटिंग र पछिल्ला साइटिंगका बीच मा न्यूनतम समय (मिनेट) बित्नुपर्छ"
    :send-usage-data.label  "अज्ञात उपयोग आंकडा पठाउने"
    :send-usage-data.description  "यो सक्षम भयो भने, अज्ञात उपयोग आंकडा कामेलोट योजनामा पठाइन्छ। यो आंकडा कामेलोट सुधार गर्ने उद्देश्यका लागि मात्र प्रयोग गरिन्छ।"
    :language.label  "भाषा"
    :language.description  "इन्टरफेस भाषा प्रयोग गरिने"
    :root-path.label  "सर्वेक्षण निर्देशिका"
    :root-path.description  "पथनाम यो सर्वेक्षणको तस्बीर आंकडाको मूल निर्देशिका  तिर। यो Camelot सर्भर प्रक्रिया  चलाउने  प्रणाली मा देखिने जस्तै हुनुपर्छ"
    :night-start-hour.label  "रात सुरु हुने समय"
    :night-start-hour.description  "त्यो समय (घण्टा) भन्दा पर जस्लाई रात मानिन्छ"
    :night-end-hour.label  "रात समाप्त हुने समय"
    :night-end-hour.description  "दिन मानिने समय"
    :project-start.label  "योजना सुरु हुने मिति"
    :project-start.description  "योजना प्रारम्भ भएको मिति।समावेशी"
    :project-end.label  "योजना समाप्त हुने मिति"
    :project-end.description  "योजना समाप्त गरिएको मिति। अनन्य"
    :surveyed-species.label  "सर्वेक्षण प्रजाति"
    :surveyed-species.description  "यो सर्वेक्षणमा समावेश भएका प्रजातिहरुको सुची"
    :required-fields.label  "आवश्यक क्षेत्रहरु"
    :required-fields.description  "मेटाडाटा मा आवश्यक हुने क्षेत्रहरुको सुचि"
    :species-name-style-scientific  "वैज्ञानिक नाम"
    :species-name-style-common  "साधारण नाम"
    :species-name-style.label  "प्रजातिको नाम शैली"
    :species-name-style.description  "पहिचान गर्दा देखाउन नाम को शैली"}

   :actionmenu
   {:title "कार्यहरू"}

   :action
   {:survey-sites  "सर्वेक्षण साइटहरू हेर्ने"
    :trap-stations  "जाल स्टेशनहरु हेर्ने"
    :sightings  "साइटिंगहरु हेर्ने"
    :maxent-report  "MaxEnt निर्यात डाउनलोड गर्ने"
    :effort-summary-report  "प्रयास सारांश डाउनलोड गर्ने"
    :summary-statistics-report  "सारांश तत्यांख डाउनलोड गर्ने"
    :species-statistics-report  "प्रजाति को तत्यांख डाउनलोड गर्ने"
    :raw-data-export  "कच्चा आंखडा निर्यात गर्ने"
    :trap-station-report  "क्यामेरा जालको तत्यांख डाउनलोड गर्ने"
    :survey-site-report  "सर्वेक्षण साइटको तत्यांख डाउनलोड गर्ने"
    :media  "मिडिया हेर्ने"
    :sessions  "सत्रहरू हेर्ने"
    :photo  "तस्बीरको मेटाडाटा हेर्ने"
    :trap-station-session-cameras  "क्यामेराहरू हेर्ने"
    :edit  "सम्पादन गर्ने"
    :delete  "मेटाउने"}

   :application
   {:import "आयात"
    :library  "पुस्तकालय"
    :organisation  "संगठन"
    :about  "बारेमा"
    :surveys  "सर्वेक्षणहरु "
    :analysis  "विश्लेषण"
    :sites  "स्थलहरू"
    :species  "प्रजातिहरू "
    :taxonomy  "प्रजातिहरू"
    :cameras  "क्यामेराहरू"
   :default-config-created "पूर्वनिर्धारित कन्फिगरेसन '%s' भित्र सिर्जना गरिएको छ"}

   :report
   {:absolute-path "तस्बीर फाइल तिर निरपेक्ष पथ"
    :nights-elapsed  "रातहरु बितेको"
    :total-nights  "रातहरु बितेको"
    :independent-observations  "स्वतन्त्र अवलोकनहरु"
    :independent-observations-per-night  "प्रशस्तत अनुक्रमणिका"
    :media-capture-date  "पकडेको मिति"
    :media-capture-time  "पकडेको समय"
    :sighting-time-delta-seconds  "अघिल्लो साइटिंग देखि को समय(सेकेन्ड)"
    :sighting-time-delta-minutes  "अघिल्लो साइटिंग देखि को समय(मिनेट)"
    :sighting-time-delta-hours  "अघिल्लो साइटिंग देखि को समय(घण्टा)"
    :sighting-time-delta-days  "अघिल्लो साइटिंग देखि को समय(दिन)"
    :time-period-start  "अवधि सुरु"
    :time-period-end  "अवधि अन्त्य"
    :percent-nocturnal  "रात(%%)"
    :presence-absence  "उपस्थिति"
    :survey-id   "सर्वेक्षण आईडी"
    :survey-name  "सर्वेक्षण नाम"
    :survey-directory "सर्वेक्षण निर्देशिका"
    :survey-notes "सर्वेक्षण टिप्पणी"
    :site-id "स्थलको आईडी"
    :site-name "स्थलको नाम"
    :site-sublocation  "स्थलको उपस्थान"
    :site-city  "स्थलको शहर"
    :site-state-province  "स्थलहरुको राज्य प्रदेश"
    :site-country  "स्थलको देश"
    :site-notes  "देश नोटहरू"
    :site-area  "स्थल क्षेत्र(कि.मी२)"
    :survey-site-id  "सर्वेक्षण स्थल आईडी"
    :taxonomy-species  "प्रजाति"
    :taxonomy-genus  "जीनस"
    :taxonomy-family  "परिवार"
    :taxonomy-order  "अर्डर"
    :taxonomy-class  "वर्ग"
    :taxonomy-label  "प्रजाति"
    :taxonomy-common-name  "साधारण नाम"
    :taxonomy-notes  "प्रजातिको नोटहरू"
    :taxonomy-id  "प्रजातिको आईडी"
    :taxonomy-updated  "वर्गीकरण अद्यावधिक गरियो"
    :taxonomy-created  "वर्गीकरण सिर्जना गरियो"
    :species-mass-id "प्रजाति जन आईडी"
    :species-mass-start  "प्रजाति जन सुरुवात"
    :species-mass-end  "प्रजाति जन अन्त"
    :camera-id  "क्यामेरा आईडी"
    :camera-name  "क्यामेराको नाम"
    :camera-make  "क्यामेरा बनाउने"
    :camera-model  "क्यामेराको नमुना"
    :camera-notes  "क्यामेराको  नोटहरू"
    :trap-station-id  "जाल स्टेशन आईडी"
    :trap-station-name  "जाल स्टेशनको नाम"
    :trap-station-longitude  "जाल स्टेशन देशान्तर"
    :trap-station-latitude  "जाल स्टेशन  अक्षांश"
    :trap-station-altitude  "जाल स्टेशन उचाइ"
    :trap-station-notes  "जाल स्टेशन नोटहरू"
    :trap-station-session-start-date  "जाल स्टेशन  सत्र सुरु हुने मिति"
    :trap-station-session-end-date  "जाल स्टेशन  सत्र अन्त्य हुने मिति"
    :trap-station-session-id  "जाल स्टेशन  सत्र आइडी"
    :trap-station-session-camera-id  "जाल स्टेशन  सत्र क्यामेरा आइडी"
    :trap-station-session-camera-media-unrecoverable  "जाल स्टेशन  सत्र क्यामेरा मिडिया अप्रापनीय"
    :media-id  "मिडिया आइडी"
    :site-count  "स्थलहरुको  संख्या"
    :trap-station-session-camera-count  "सत्रहरु मा क्यामेराहरूको संख्या"
    :trap-station-session-count  "जाल स्टेशन  सत्रहरुको संख्या"
    :trap-station-count  "जाल स्टेशनहरुको संख्या"
    :media-count  "तस्बीरहरुको संख्या"
    :species-name  "प्रजातिको नाम"
    :taxonomy-count  "मिडिया पक्रने टाइमस्ट्याम्प"
    :media-capture-timestamp  "मिडिया नोटहरू"
    :media-notes  "मिडिया फाइलको नाम"
    :media-filename  "मिडियाको ढाँचा"
    :media-format  "मिडियामा ध्यान आवश्यक"
    :media-attention-needed  "मिडिया क्यामेरा जाँच"
    :media-cameracheck  "मिडिया प्रशोधित"
    :media-processed  "मिडिया सिर्जना गरियो"
    :media-created  "मिडिया अद्यावधिक गरियो"
    :media-updated  "मिडिया URI"
    :media-uri  "साइटिंग आइडि"
    :sighting-id  "साइटिंग सिर्जना गरियो"
    :sighting-created  "साइटिंग अद्यावधिक गरियो"
    :sighting-updated  "साइटिंग मात्रा"
    :sighting-quantity  "तस्बीरको ISO सेटिंग"
    :photo-iso-setting  "तस्बीरको एक्सपोजर मान"
    :photo-exposure-value  "तस्बीरको फ्ल्यास सेटिंग"
    :photo-flash-setting  "तस्बीरको फोकल लम्बाई"
    :photo-focal-length  "तस्बीरको एफ संख्या सेटिंग"
    :photo-fnumber-setting  "तस्बीरको अभिमुखीकरण"
    :photo-orientation "Photo Orientation"
    :photo-resolution-x  "तस्बीरको X- रिजोलुसन (पिक्सेल)"
    :photo-resolution-y "तस्बीरको Y - रिजोलुसन (पिक्सेल)"}

   :camelot.report.module.builtin.reports.camera-traps
   {:title "[camtrapR] क्यामेरा  जाल निर्यात"
    :description "क्यामेरा जाल विवरणको  एउटा CamtrapR-मिल्दो निर्यात। Camtrap मा आयात गर्दा 'byCamera' लाई True मा सेट गर्ने"}

   :camelot.report.module.builtin.reports.effort-summary
   {:title  "प्रयासको सारांश"
    :description  "सर्वेक्षण र जाल स्टेशनको स्थलहरु breakdown गरियो"}

   :camelot.report.module.builtin.reports.full-export
   {:title "पूर्ण निर्यात"
    :description  "Camelot मा एक पंग्ति प्रति अद्वितीय रेकर्ड मा आंकडा निर्यात गर्ने"}

   :camelot.report.module.builtin.reports.survey-export
   {:title "सर्वेक्षण निर्यात"
    :description  "Camelotको सर्वेक्षण मा एक पंग्ति प्रति  अद्वितीय रेकर्ड मा आंकडा निर्यात गर्ने"}

   :camelot.report.module.builtin.reports.raw-data-export
   {:title "कच्चा आंकडा निर्यात"
    :description  "प्रत्येक अपलोड गरिएको तस्बिरको बारेमा विवरण"}

   :camelot.report.module.builtin.reports.species-statistics
   {:title "प्रजातिको तथ्याड़क"
    :description  "सबै सर्वेक्षण मा एउटा प्रजातिकोलागि स्थलहरुको breakdown"}

   :camelot.report.module.builtin.reports.summary-statistics
   {:title "सारांश तथ्याड़क"
    :description  "सर्वेक्षणमा प्रत्येक प्रजातिको अवलोकनका लागि सारांश रिपोर्ट"}

   :camelot.report.module.builtin.reports.survey-site
   {:title "सर्वेक्षण स्थल तथ्याड़क"
    :description  "सर्वेक्षण स्थलमा प्रत्येक प्रजातिको अवलोकन"}

   :camelot.report.module.builtin.reports.trap-station
   {:title "जाल स्टेशनको तथ्याड़क"
    :description  "एउटा दिइएको जाल स्टेशन मा अवलोकनहरु र ती अवलोकनहरु जम्मा गर्दा र्बितेको समय"}

   :camelot.report.module.builtin.reports.record-table
   {:title "[CamtrapR ] रेकर्ड टेबल"
    :description "स्वतन्त्र साइटिंगहरु को एउटा CamtrapR संग मिल्दो रेकर्ड टेबल निर्यात"
    :media-directory "माध्यम निर्देशिका"}

   :camelot.report.module.builtin.reports.occupancy-matrix
   {:title-count "अक्युपेन्सी म्याट्रिक्स -प्रजाति गणना"
    :description-count  "एउटा प्रजातिको प्रति दिन प्रति जाल स्टेशन स्वतन्त्र साइटिंगहरुको अक्युपेन्सी म्याट्रिक्स. PRESENCE संग प्रयोग गर्न उपयुक्त"
    :title-presence "अक्युपेन्सी म्याट्रिक्स - प्रजातिको उपस्थिति"
    :description-presence "अक्युपेन्सी म्याट्रिक्स - कुनै प्रजाति एक निश्चित दिनमा ट्रयाप स्टेशनमा उपस्थित थियो कि थिएन भनेर देखाउँदै. PRESENCE संग प्रयोग गर्न उपयुक्त"
    :start-date  "सुरु हुने  मिति"
    :start-date-description  "म्याट्रिक्स  सुरु हुने मिति ,समावेशी "
    :end-date  "अन्त्य हुने मिति"
    :end-date-description  "म्याट्रिक्स  अन्त्य गर्ने मिति ,समावेशी"}

   :camelot.import.core
   {:timestamp-outside-range "समयछापहरु सत्र मिति भन्दा बाहिर छन् "}

   :camelot.component.camera.core
   {:new-camera-name-placeholder "नयाँ क्यामेराको नाम"
    :invalid-title  "क्यामेराको नाम दिइएको थिएन कि चाहिँ त्यो नाम को क्यामेरा पहिले नै   अवस्थित थियो"
    :filter-cameras  "फिल्टर क्यामेराहरु"
    :confirm-delete  "के तपाईँ निश्चित हुनुहुन्छ कि तपाईँलाई यो क्यामेरा मेटाउन मन छ? यसले कुनै सम्बन्धित माध्यम पनि मेटाइदिन्छ।"
    :blank-filter-advice  "तपाईँले क्यामेराहरुलाई तलको निवेश छेत्र प्रयोग गरेर थप्न सक्नु हुन्छ"
    :blank-item-name  "क्यामेराहरू"}

   :camelot.component.camera.manage
   {:validation-failure-title "बुझाउनु भन्दा पहिला, माथिको त्रुटिहरू सच्याउनुहोस्"
    :update-camera  "क्यामेरा अद्यावधिक गर्नुहोस्"
    :trap-station-column  "जाल स्टेशन"
    :trap-station-session-start-date-column  "सत्र सुरु हुने मिति"
    :trap-station-session-end-date-column  "सत्र अन्त हुने मिति"}

   :camelot.component.library.core
   {:delete-media-question "के तपाईंलाई साँचै छनौट गरेको माध्यम मेटाउन मन छ ?"
    :delete-media-title  "मेटाउने   निश्चित गर्ने"
    :delete-sightings-question  "के तपाईंलाई साँचै छनौट गरेको माध्यमबाट सबै साइटिंगहरु मेटाउन मन छ?"
    :delete-sightings-title  "मेटाउने   निश्चित गर्ने"
    :media-select-count  "हाल छनौट गरिएको मेटाउने वस्तुहरुमा %d वटा वस्तुहरु छन्"
    :sighting-select-count  "हाल छनौट गरिएको मेटाउने वस्तुहरुमा %d वटा साइटिंगहरु छन्"}

   :camelot.component.library.collection
   {:upload-advice  "क्यामेरा जाल स्टेशनहरु थपेर , त्यसलाई क्यामेरा जाँच थपेर  सर्वेक्षणमा  माध्यम हालेर, माध्यम  थप्ने "
    :filter-notice  "मिल्दो माध्यम भेटियेना"
    :filter-advice  "त्यो त लज्जास्पद्द् कुरा भयो। शायद अर्को खोज गर्ने?"
    :select-none-button-title  "सबै छनौट हटाउने"
    :select-none-button  "केहि पनि छनौट नगर्ने"
    :select-all-button-title  "यो पृष्ठ को सबै माध्यम छनौट गर्ने"
    :select-all-button  "सबै छनौट गर्ने"
    :item-name  "माध्यम"
    :identify-selected  "छनौट भएको पहिचान गर्ने"
    :reference-window-no-media-notice  "सन्दर्भ गुणस्तरको तस्बीर भेटिएन"
    :reference-window-no-media-advice  "भविष्यमा,प्रजाति पहिचान गर्न उपयोगी हुने तस्बीरहरुलाई 'सन्दर्भ गुणस्तर' भनेर मुख्य झ्याल मा  चिनो लगाउन सकिन्छ ,र यो भविष्यमा यहाँ देखिन्छ"
    :selected-media-from-multiple-surveys  "छनौट गारिएको माध्यम पहिचान गर्नको लागि तिनीहरू एउटा सर्वेक्षणसंग मात्र सम्बन्धित हुन पर्छ"}

   :camelot.component.library.identify
   {:identify-selected "पहिचान  छनौट"
    :taxonomy-add-placeholder  "नयाँ प्रजाति को नाम"
    :species-format-error  "प्रजातिहरुको लागि अवैध वैज्ञानिक नाम"
    :add-new-species-label  "एउटा नयाँ प्रजाति थप्नुहोस"
    :submit-not-allowed  "एक वा बढी क्षेत्रहरू, तिनीहरुको वर्तमान स्तिथिमा बुझाउन मिल्दैन"}

   :camelot.component.library.util
   {:reference-quality  "सन्दर्भ गुणस्तर"
    :ordinary-quality  "साधारण गुण"
    :identified  "पहिचान गरिएको"
    :test-fires  "परीक्षण आगो"
    :not-test-fires  "अब उप्रान्त परीक्षण आगो छैन"
    :flagged  "झण्डा लगाइएको"
    :unflagged  "झण्डा नलगाइएको"
    :processed  "प्रशोधित"
    :unprocessed  "अप्रशोधित"
    :selected  "छनौट गरिएको"}

   :camelot.component.library.preview
   {:photo-not-selected "तस्बीर छनौट गरिएको छैन"
    :species-not-in-survey  "प्रजाति यो सर्वेक्षणमा छैन"
    :brightness-label  "चमक"
    :contrast-label  "कन्ट्रास्ट"
    :sightings  "साइटिंगहरु"
    :delete-media  "छनौट गरेको माध्यमहरु मेटाउने"
    :delete-sightings  "छनौट गरेको माध्यमबाट साइटिंगहरु हटाउने"}

   :camelot.component.library.search
   {:reference-window-button-text "सन्दर्भ झ्याल"
    :reference-window-button-title  "पहिचानका लागि  हाल छनौट भएका   प्रजातिहरुको   सन्दर्भ- गुणस्तरको तस्बीर थप झ्यालले प्रदर्शन  गर्छ"
    :filter-button-title  "हालका फिल्टरहरू निवेदन गर्ने"
    :filter-placeholder  "खोजी गर्ने"
    :filter-title  "एउटा कुञ्जी शब्द टाइप गर्नुहोस् जुन तपाईंलाई माध्यममा समावेश गर्न मन छ"
    :filter-survey-title  "कुनै एउटा सर्वेक्षणको वस्तुहरू मा मात्र फिल्टर गर्ने"
    :filter-survey-all-surveys  "सबै  सर्वेक्षणहरु"
    :reference-window-partial-title  "सन्दर्भ तस्बीरहरु"
    :identification-panel-button-title  "छनौट गरेको माध्यम निवेदन गर्न परिचय प्यानल खोल्नुहोस्"
    :identification-panel-button-text  "छनौट गरेको पहिचान गर्नुहोस्"
    :filter-trap-station-all-traps  "सबै जालहरु"
    :flag-media-title  "छनौट गरेको माध्यममा झन्डा लगाउने वा नलगाउने आवश्यकता अनुसार ध्यान दिने"
    :media-cameracheck-title  "छनौट गरेको माध्यमलाई मानवको कारण परिक्षण आगो को रुपमा चिन्ह लगाउने"
    :media-processed-title  "छनौट गरेको माध्यमलाई प्रशोधित वा अप्रशोधित को रुपमा सेट गर्ने"
    :media-reference-quality-title  "छनौट गरेको माध्यम उच्च गुणस्तरको हो भन्नॆ संकेत गर्दछ र यस्लाई सन्दर्भको रुपमा प्रयोग गर्न पर्छ"
    :filter-unprocessed-label  "अप्रशोधित"
    :filter-animals-label  "जनावर छ?"}

   :camelot.component.albums
   {:num-nights "%d रातहरु"
    :num-photos  "%d तस्बीरहरु"
    :timestamp-information-missing  "समयअवधि जानकारी छुटेको"
    :import-validation-error-title  "प्रमाणीकरण त्रुटिहरूले गर्दा आयात गर्न सकिएन"
    :import-warning-confirm  "यो फोल्डरमा दोषी आंकडाहरु हुन सक्छ। यो आयात गर्नाले भविष्यको विश्लेषणको शुद्धतामा सम्झौता हुन सक्छ। के तपाइँ जारी राख्न चाहानुहुन्छ?"
    :no-problems  "कुनै समस्या भेटिएन। विश्लेषण गर्ने समय!"
    :loading  "आंकडा लोड हुँदैछ"}

   :camelot.component.deployment.camera-check
   {:help-text  "हरेक पटक क्यामेरा जाल हेर्न जाँदा क्यामेरा जाँच रेकर्ड गर्नुहोस्। क्यामेरा जाल तैनाती स्वत सकिन्छ जब त्यहाँ  'सक्रिय' क्यामेराहरु   तोकिएको हुँदैन।"
    :no-cameras  "क्यामेराहरू उपलब्ध छैन"
    :no-replacement-camera  "प्रतिस्थापन क्यामेरा छैन"
    :status-question  "यो क्यामेरा को नयाँ स्थिति के छ ?"
    :replacer-question  "कुन क्यामेराले यस्लाई  प्रतिस्थापित गर्ह्यो, यदि कुनै छ  भने?"
    :media-retrieved  "माध्यम  पुनःप्राप्त"
    :media-recovered  "माध्यम  पुनःप्राप्त भयो"
    :media-not-recovered  "माध्यम पुनःप्राप्त हुन सकेन"
    :camera-check-date  "क्यामेरा -जाँच मिति"
    :date-validation-past  "हालको  सत्रको मिति भन्दा पहिला  सुरु  मिति हुन सक्दैन।"
    :date-validation-future  "मिति भविष्यमा हुन सक्दैन"
    :primary-camera  "प्राथमिक क्यामेरा"
    :secondary-camera  "माध्यमिक क्यामेरा"
    :add-secondary-camera  "माध्यमिक क्यामेरा थप्नुहोस"
    :secondary-camera-label  "माध्यमिक क्यामेरा ,यदि कुनै हो भने"
    :validation-same-camera  "माध्यमिक क्यामेरा,प्राथमिक क्यामेरा  जस्तो हुन हुँदैन"
    :finalised-trap-notice  "यो क्यामेरा जाल टुंगिएको छ"
    :finalised-trap-advice  "Camelot ले स्वत क्यामेरा जाल स्थल टुंग्याउछ जब त्यहाँ कुनै सक्रिय क्यामेरा रहदैन। यो स्थानमा क्यामेरा जाल निरन्तर राख्न ,नयाँ क्यामेरा जाल स्टेशन  बनाऊँनुहोस।"}

   :camelot.component.deployment.core
   {:primary-camera-name "क्यामेराको नाम(प्राथमिक)"
    :secondary-camera-name  "क्यामेराको नाम (माध्यमिक)"
    :record-camera-check  "रेकर्ड क्यामेरा जाँच"
    :start-date  "सत्र सुरु हुने मिति"
    :end-date  "सत्र अन्त हुने मिति"
    :blank-item-name-lc  "क्यामेरा जाल स्टेशनहरु"
    :advice-context  "यिनीहरु क्यामेरा तैनात भएका स्थान हुन्"
    :advice-direction  "तपाईंले तलको बटन प्रयोग गरेर केहि माथि सेट गर्न सक्नु हुन्छ"
    :create-title  "एउटा नयाँ क्यामेरा जाल तैनाथ थप्नुहोस"
    :create-button "क्यामेरा जाल थप्नुहोस "
    :confirm-delete  "के तपाईँ यो क्यामेरा जाल स्टेशन र यसमा भएका कुनै माध्यम,यो संग सम्बन्धित क्यामेरा  मेटाउन  निश्चित हुनुहुन्छ ?"
    :finalised "टुङ्गियो"}

   :camelot.component.deployment.create
   {:new-camera-name-placeholder  "नयाँ क्यामेराको नाम"
    :camera-invalid-title  "यो नाम को क्यामेरा पहिले नै अवस्थित छ"
    :create-new-camera  "नयाँ क्यामेरा बनाउनुहोस्"
    :new-site-name-placeholder  "नयाँ स्थान को नाम"
    :site-invalid-title  "स्थान को नाम प्रदान गरिएको छैन वा यो नामको स्थान पहिले नै अवस्थित छ"
    :create-new-site  "नयाँ स्थान बनाउनुहोस"
    :start-date  "सुरु हुने मिति"
    :validation-future-date  "मिति भविष्यमा हुन सक्दैन"
    :invalid-latitude  "अक्षांश [ -९० ,९० ] को दायरामा हुनु  पर्छ"
    :invalid-longitude  "देशान्तर [-१८०,१८० ] को दायरामा हुनु पर्छ"
    :primary-camera  "प्राथमिक क्यामेरा"
    :secondary-camera  "माध्यमिक क्यामेरा"
    :validation-same-camera  "माध्यमिक क्यामेरा ,प्राथमिक क्यामेरा जस्तो हुनु हुँदैन"
    :validation-failure  "कृपया सबै आवश्यक क्षेत्रहरु पूरा गर्नुहोस् र कुनै त्रुटिहरू सम्बोधन गर्नुहोस्"
    :add-camera-trap  "क्यामेरा जाल स्थान थप्नुहोस"
    :edit-camera-trap  "क्यामेरा जाल स्थान सम्पादन गर्नुहोस्"}

   :camelot.component.progress-bar
   {:progress-bar-title "%d पूर्ण ,%d असफल, र %d उपेक्षित"}

   :camelot.component.deployment.recent
   {:help-text "माध्यम फाईलहरू थप्न क्यामेरा जाँचमा तानेर खसाल्नुहोस् "
    :format-not-supported  "%d समर्थित ढाँचा होइन"
    :upload-error  "अपलोड गर्दा त्रुटि"
    :gps-coordinates  "GPS निर्देशांक"
    :show-details  "विवरण देखाउनुहोस"
    :blank-item-name  "क्यामेरा जाँचहरु"
    :media-uploaded  "माध्यम अपलोड गरियो"
    :confirm-delete  "के तपाईँ निश्चित हुनुहुन्छ,  तपाईँलाई यो क्यामेरा जाँच मेटाउन मन छ?"
    :confirm-delete-has-media  "के तपाईँ निश्चित हुनुहुन्छ, तपाईँलाई यो क्यामेरा जाँच मेटाउन मन छ? यस्ले कुनै पनि सम्बन्धित माध्यम मेटाउछ "
    :blank-advice  "यो ,तपाईले क्यामेरा जाल स्थानमा जाँच थपे पछि  देखा पर्दछ"}

   :camelot.component.deployment.shared
   {:sort-by "द्वारा क्रमबद्ध"
    :start-date  "सुरु मिति"
    :end-date  "अन्त मिति"}

   :camelot.component.notification
   {:problems "त्यहाँ केही समस्याहरू थिए"
    :generic-error  "अपरेशन पूरा हुन सकेन। यो निवेश मान्य नभएको कारणले हुन सक्छ| त्रुटि को विवरण तल छन्"
    :page-not-found "यो नामको पाना  देखिएको छैन"
    :maybe-bug "तपाइलाई यो गल्ति लाग्छ भने "
    :report-issue "समस्या ब्यक्त गर्नुहोस "
    :ask-on-forum "फोरममा सोध्नुहोस "}

   :camelot.component.import-dialog
   {:import-from "आयात गर्ने "
    :import-media "माध्यम आयात गर्नुहोस "}

   :camelot.component.organisation
   {:not-implemented "माफ गर्नुहोस, यो अहिले बनिसकेको छैन "
    :organisation "तपाइको संगठन"
    :cameras "क्यामेराहरू "
    :surveys "सर्वेक्षणहरु "
    :sites "स्थलहरु"
    :reports "रिपोर्टहरु "
    :backup "ब्याकअप / पुनर्स्थापना"}

   :camelot.component.species-search
   {:scientific-name "वैज्ञानिक नाम..."
    :search-species "प्रजातिलाई वैज्ञानिकले खोज्नुहोस "}

   :camelot.component.util
   {:blank-notice-template "अहिलेसम्म कुनै % छैनन् "
    :use-button-below "तलको बत्टन प्रयोग गरेर केहि मिलाउन सक्नुहुन्छ"
    :use-advanced-menu "तलको 'उन्नत' मेनू प्रयोग गरेर केहि मिलाउन सक्नुहुन्छ "}

   :camelot.component.report.core
   {:filter-reports-placeholder "रिपोर्टहरु छान्नुहोस्..."
    :item-name "रिपोर्टहरु"
    :notice "कुनैपनि  रिपोर्टहरु मिलेनन   "
    :advice "यो खोजको केहि परिणामहरू थिएनन."}

   :camelot.component.site.core
   {:new-site-name "नया स्थलको नाम..."
    :validation-duplicate-site "यो नामको स्थल पहिलै छ"
    :confirm-delete "के तपाइ यो स्थल पक्का मेटाउन चाहनुहुन्छ? यसो गर्दा सम्बन्धित माध्यमहरु पनि मेटिनेछन्"
    :item-name "स्थलहरु "
    :advice "तपाई तलको इनपुट क्षेत्र प्रयोग गरेर स्थलहरु थप्न सक्नुहुन्छ"
    :filter-sites "स्थलहरु छान्नुहोस्..."}

   :camelot.component.site.manage
   {:validation-site-name "यो खाली वा अर्को स्थलको जस्तै नाम भएको हुन हुदैन "
    :validation-failure "बुझाउनु भनदा अगाडी माथिको त्रुटीहरु सच्चाउनुहोस "
    :default-intro "स्थल अद्यावधिक गर्नुहोस्"}

   :camelot.component.species.core
   {:item-name "प्रजातिहरू "
    :manage-species "प्रजातिहरू प्रबन्ध गर्नुहोस् "
    :confirm-delete "के तपाइ यो प्रजातिलाई यस सर्वेक्षणबाट पक्का हटाउन चाहनुहुन्छ?"}

   :camelot.component.species.manage
   {:search-instructions "दायाँको विकल्पहरु प्रयोग गरेर प्रजातिहरू खोज्नुहोस र थप्नुहोस."
    :new-species-name-placeholder "नया  प्रजातिहरूको नाम..."
    :validation-duplicate-species "यो नामको प्रजाति पहिलै छ "
    :new-or-existing "नया वा पहिलै रहेको  प्रजाति "
    :new-species "नया  प्रजाति थप्नुहोस..."
    :expected-species "अपेक्षित प्रजाति "
    :intro "प्रजाति प्रबन्ध गर्नुहोस् "}

   :camelot.component.species.update
   {:validation-error-title "बुझाउनु अगाडी सबै क्ष्यत्रहरु पूर्ण गर्नुहोस"
    :update-species "प्रजाति अद्यावधिक गर्नुहोस् "}

   :camelot.component.survey.core
   {:create-survey "सर्वेक्षण सिर्जना गर्नुहोस् "
    :manage-traps "क्यामेरा स्टेशनहरु प्रबन्ध गर्नुहोस् "
    :upload-captures "माध्यम अपलोड गर्नुहोस् "
    :species "प्रजातिहरू "
    :files "सम्बन्धित फाइलहरु "
    :import "ठुलो परिमाण आयात  "
    :settings "सेटिंग्स "}

   :camelot.component.survey.settings
   {:details "सर्वेक्षण विवरण "
    :sighting-fields "सैटिंग क्ष्यत्रहरु "
    :delete-survey "सर्वेक्षण मेटाउनहोस् "
    :confirm-delete "के तपाइ यो  सर्वेक्षण पक्का मेटाउन चाहनुहुन्छ? यसो गर्दा सम्बन्धित माध्यमहरु र फाइलहरु  पनि मेटिनेछन् "
    :survey-name-placeholder "सर्वेक्षण नाम..."
    :validation-error-title "अद्यावधिक गर्नु अगाडी सबै क्ष्यत्रहरुमा मान्य सामग्री छन् भनेर पक्का गर्नुहोस "
    :validation-survey-name-blank "सर्वेक्षणको  नाम खाली हुनु हुदैन "
    :validation-survey-name-duplicate "यस सर्वेक्षणको नाम अर्को सर्वेक्षणको जस्तै हुनु हुदैन "
    :validation-survey-notes-blank "सर्वेक्षणको विवरण खाली हुनु हुदैन "
    :validation-survey-sighting-independence-threshold-not-a-number "स्वतन्त्रता थ्रेसोल्ड अंक हुनुपर्छ "}

   :sighting-field
   {:sighting-field-label.label "साईटिंग क्ष्यत्रको लेबल  "
    :sighting-field-label.description "साईटिंग क्ष्यत्रको लेबल  "
    :sighting-field-key.label "साईटिंग क्ष्यत्रको कुञ्जी "
    :sighting-field-key.description "साईटिंग क्ष्यत्रलाई पहिचान गर्ने कुञ्जी. क्ष्यत्रहरुलाई सहकार्य गरौनलाई सर्वेक्षणहरु पार एउतै  कुञ्जी गर्ने "
    :sighting-field-datatype.label "साइटिंग क्ष्यत्रको  प्रकार "
    :sighting-field-datatype.description "बनाउनु पर्ने इनपुट क्ष्यत्रको  प्रकार. यस चनावटले क्ष्यत्रको व्यवहारलाई असर गर्छ "
    :sighting-field-required.label "साइटिंग क्ष्यत्र चाहिन्छ?"
    :sighting-field-required.description "साइटिंग बुझाउनु भन्दा अगाडी क्ष्यत्रमा मान भए पनि नभए पनि,  साइटिंग बुझाउनु सकिन्छ"
    :sighting-field-default.label "पूर्वनिर्धारित साइटिंग क्ष्यत्र  "
    :sighting-field-default.description "नया साईटिंगमा प्रयोगहुने  पूर्वनिर्धारित मान"
    :sighting-field-affects-independence.label "साइटिं क्ष्यत्रले स्वतन्त्रतालाई असर गर्छ ?"
    :sighting-field-affects-independence.description "कुनै रिपोर्टहरुमा, Camelotले आश्रित सोचिने साइटिंगलाई फिल्टर गर्छ| साइटिं स्वतनत्रता सम्बन्धित विवरणको लागि Camelotको कागजात परामर्श गर्नुहोस|"
    :sighting-field-ordering.label "साइटिं क्ष्यत्रको मर्यादाक्रम "
    :sighting-field-ordering.description "यो फारममा ,अरु क्ष्यत्रको आधारमा यस क्ष्यत्रको रखाई"}

   :camelot.component.survey.sighting-fields
   {:page-title "साइटिंग क्ष्यत्रहरु "
    :new-field "नया क्ष्यत्र "
    :new-field-from-template "टेम्पलेटबाट नया क्ष्यत्र "
    :sighting-field-label.label "लेबल "
    :sighting-field-label.description "साइटिंग क्ष्यत्रको लेबल"
    :sighting-field-key.label "पहिचान कुञ्जी"
    :sighting-field-key.description "साइटिंग क्ष्यत्रहरु पहिचान गर्ने  कुञ्जी। सर्वेक्षणपार एउटै कुञ्जी प्रयोग गर्दा हुन्छ।"
    :sighting-field-datatype.label "क्ष्यत्र प्रकार "
    :sighting-field-datatype.description "इनपुट गर्ने क्ष्यत्रको प्रकार. यस चनावटले क्ष्यत्रको व्यवहारलाई असर गर्छ."
    :sighting-field-options.label "विकल्पहरू छान्नुहोस् "
    :sighting-field-required.label "चाहिन्छ?"
    :sighting-field-required.description "साइटिंग बुझाउनु भन्दा अगाडी क्ष्यत्रमा मान भए पनि नभए पनि,  साइटिंग बुझाउनु सकिन्छ"
    :sighting-field-default.label "पूर्वनिर्धारित मान "
    :sighting-field-default.description "नया साईटिंगमा प्रयोगहुने  पूर्वनिर्धारित मान"
    :sighting-field-affects-independence.label "स्वतनत्रतालाई असर गर्दछ?"
    :sighting-field-affects-independence.description "कुनै रिपोर्टहरुमा, Camelotले आश्रित सोचिने साइटिंगलाई फिल्टर गर्छ|साईटिंग स्वतनत्रता सम्बन्धित विवरणको लागि Camelotको कागजात परामर्श गर्नुहोस"
    :sighting-field-ordering.label "मर्यादाक्रमहरु "
    :sighting-field-ordering.description "यो फारममा ,अरु क्ष्यत्रको आधारमा यस क्ष्यत्रको रखाई"
    :delete-question "के तपाइ यो साइटिंग क्ष्यत्र  पक्का मेटाउन चाहनुहुन्छ? यसो गर्दा यस क्ष्यत्रसंग सम्बन्धित डाटा पनि मेटिनेछन् "
    :add-option "विकल्पहरू थप्नुहोस "
    :template "टेमप्लेट छान्नुहोस् "
    :default-template "पूर्वनिर्धारित "
    :help-text "पहिचानको क्रममा थप जानकारीलिनका लागी साईटिंग क्ष्यत्रहरु प्रयोग गरीन्छन "}

   :camelot.component.bulk-import.core
   {:download "CSV बनाउनुहोस "
    :title "ठुलो परिमाण आयात "
    :survey-menu "सर्वेक्षण मेनु "
    :ready-to-upload "CSV"
    :path-name-placeholder "एक फोल्डरको  निरपेक्ष वा सापेक्ष मार्ग..."
    :help-text-step-1 "चरण १. सर्वेक्षण फोलडरमा भएको सबै डेटाको स्वचालित रूपमा CSV बनाउनुहोस "
    :help-text-step-2 "चरण २. तपाइलाई ठिकलाग्ने हिसाबमा लाई परिमार्जन गर्नुहोस्, अनि तपाई तयार हुनुभयोपछि  कयातको प्रक्रिया सुरु गर्नुहोस "
    :survey-directory "सर्वेक्षण निर्देशिकाको पथ "}

   :camelot.import.validate
   {:camera-overlap "%s बहु सत्रहरुको माझ प्रयोग भएको छ %s."
    :filesystem-space "आयातको लागि डिस्कमा  %d %s  ठाउँ चाहिन्छ, तर  %d %s मात्र उपलब्ध छ"
    :session-dates "माध्यम सत्र मिति भित्र भएन। CSV row: %d."
    :check-sighting-assignment "एउटा साइटिंग बनाईएको हुनुपर्छ, तर सबै क्ष्यत्रहरुमा मान छैनन्। CSV row: %d."
    :future-timestamp "सत्र अन्त्य हुने मिति भविष्यमा छ। CSV row: %d."
    :session-start-before-end "सत्र अन्त्य हुने मिति सुरु हुने भन्दा अगाडी छ | CSV row: %d."}

   :camelot.component.bulk-import.mapper
   {:title "ठुलो परिमाण आयात "
    :scanning "डेटालाई म्यापि हर्न तयारी गर्दै..."
    :import-status-dialog-title "आयात गरिदै..."
    :validation-mismatch "तोकिएको  म्यापि सन्तुष्टि हुनको लागि एक वा बढी क्ष्यत्रमा डेटा छैनन्  "
    :import-started "ठुलो परिमाण आयात सुरु भयो।  आयात भैराख्दा तपाई चलाईराख्नन सक्नुहुन्छ र तलको तस्बिरमा देखाए जस्तो , माथिको दाँया कुनाको  मेनू प्रयोग गरेर यसको स्थितिकुनै पनि समयमा हेर्न सक्नुहुन्छ  "
    :progress-bar-label "आयातको  प्रगति"
    :import-failed "ठुलो परिमाण आयात सुरु भएन. यो गल्ति हुन सक्छ। यो त्रुटिको  प्राविधिक विवरण निम्नानुसार छ:"
    :validation-problem "आयात गरिनु पर्ने डेटामा केहि असंगतिहरू भेटिएका छन्।  त्यस कारणले आयात रद्द गरिएको छ। यी असंगतिहरू तल विवरण गरिएको छ।"
    :sample-ui "UI  को एउटा उदाहरण तस्बिर।"
    :initialising "ठुलो परिमाण आयात तयार गरिदै।  कृपया धैर्य गर्नुहोस।"
    :please-wait "आयात सुरुहुनको लागि कृपया धैर्य गर्नुहोस। "
    :status-code "स्थिति कोड "
    :validation-missing "एक वा बढी क्ष्यत्रमा म्यापि तोकिएको छैनन् "
    :sightings-partially-mapped "एक वा बढी साइटिंग क्ष्यत्रहरु मिलाइएको छ, तर सबै वा कुनै पनि हनहुने छनौट गर्नु पर्छ। साइटिंग क्ष्यत्रहरु यो : :%s हुन्। "
    :validation-passed "आयातको लागि म्यापि बुझौनुहोस "
    :invalid-csv "फिले अपलोड गर्न सकिएन।  यो मान्य CSV हो भनेर जाँच गर्नुहोस् अनि फेरी प्रयास गर्नुहोस्। "}

   :camelot.component.survey.create
   {:search-instructions "दाँयाको विकल्पहरू प्रयोग गरेर प्रजाति खोजनुहोस् र थप्नुहोस "
    :survey-name "सर्वेक्षणको नाम "
    :survey-name-placeholder "सर्वेक्षणको नाम..."
    :survey-description "सर्वेक्षणको वर्णन "
    :expected-species "अपेक्षित प्रजाति "
    :create-survey "सर्वेक्षण सिर्जना गर्नुहोस्"
    :intro "सर्वेक्षण सिर्जना गर्नुहोस् "
    :submit-title "यो सर्वेक्षणलाई  बुझाउनुहोस "
    :validation-error-title "बुझाउनु अगाडी सबै आवश्यक क्ष्यत्रहरु पूर्ण गर्नुहोस। "}

   :camelot.component.survey.file
   {:upload-time "अपलोडको समय "
    :upload-file "अपलोडको फाइल "
    :file-size "फाइलको आकार  "
    :item-name "फाइलहरु "
    :advice "यदि तपाईं चाहनुहुन्छ, तपाई तल केहि उपलोड गर्न सक्नुहुन्छ। "
    :confirm-delete "के तपाइ यो फाइल पक्का  मेटाउन चाहनु हुन्छ ?"}

   :camelot.util.model
   {:schema-not-found "column '%s' को लागि कुनै  रूपरेखा भेटिएन।  "
    :datatype-and-required-constraint-problem "All records for this field should be %s and a value should always be present, but this is not the case."
    :datatype-problem-only "यस क्ष्यत्रको सबै रेकर्डहरु %s र एउटा मन सधै उपस्थित हुनुपर्छ, तर यस्तो छैन।"
    :required-constraint-problem-only "एउटा मन सधै उपस्थित हुनुपर्छ, तर यस्तो छैन।"
    :max-length-problem "यो क्ष्यत्रको अधिकतम इनपुट लम्बाई %d हुनु पर्छ तर इनपुटको अधिकतम लम्बाई %d छ। "
    :calculated-schema-not-available "Column  %s को metadata निर्धारण गर्न सकिएन। "
    :datatype-integer "एक पूर्णांक"
    :datatype-number "एक संख्या"
    :datatype-timestamp "एक समयछाप "
    :datatype-longitude "[-180, 180] दायरामा मान "
    :datatype-latitude "[-90, 90] दायरामा मान "
    :datatype-boolean "कि ‘सत्य’ वा ‘गलत’ "
    :datatype-file "फाइलमा पुग्ने पथ "
    :datatype-string "अक्षर चरित्र "
    :datatype-date "एक मिति "}

   :datatype
   {:text "पाठ इनपुट "
    :textarea "बहु-रेखा पाठ इनपुट "
    :number "संख्या "
    :select "ड्रप डाउन "
    :checkbox "चेकबाकस "}

   :camelot.component.nav
   {:bulk-import-progress-label "ठुलो परिमाण आयातको प्रगति"
    :bulk-import-status "चलिरहेको ठुलो परिमाण आयातको  विवरण."
    :bulk-import-failures "कुनै रेकोर्डहरु आयातहुन असफल भए.  थप जानकारीका लागि यहाँ थिच्नुहोस "
    :bulk-import-success "सबै माध्यमहरु सफलतापूर्वक आयात भएका छन्। "
    :bulk-import-complete "आयात  पूर्ण भयो "
    :bulk-import-complete-with-errors "आयात  पूर्ण भयो (त्रुटिहरूको साथ)"
    :bulk-import-cancelled "आयात रद्द गरियो"
    :bulk-import-calculating "बाँकि रहेको समय हिसाब गर्दै ..."
    :bulk-import-time-remaining "बाँकि रहेको समय अनुमान गर्दै "
    :bulk-import-failed-paths "निम्न फाईलहरू आयात गर्न असफल भयो"}

   :camelot.model.trap-station-session
   {:trap-station-session-closed-label "%s देखि %s"
    :trap-station-session-ongoing-label "%s र  हाल चलिरेहेको "}

   :camelot.import.template
   {:directory-not-found "%s: निर्देशिका भेटिएन "
    :directory-not-readable "%s: निर्देशिका पढ्न मिल्दैन "
    :no-media-found "%s: कुनै माध्यम भेटिएन "}

   :camelot.component.about
   {:title "बारेमा जानकारी "}

   :camelot.validation.validated-component
   {:not-empty "खाली हुन हुदैन "
    :not-keyword "कुनै तल्लोकेस अक्षरांकीय पात्रहरू र हाइफन ('-') हुन हुदैन"
    :too-long "%d पात्रहरूभन्दा लामो हुन हुदैन "
    :not-distinct "पहिले नै प्रयोगमा हुनुहुन्न "}

   :missing "[ अनुवाद नभएको अवस्था]"})
