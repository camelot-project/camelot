(ns camelot.spec.model.trap-station-session-camera
  (:require [clojure.spec.alpha :as s]
            [camelot.spec.model.trap-station-session :as trap-station-session]
            [clj-time.spec :as ts]))

(s/def ::trap-station-session-camera-id int?)
(s/def ::trap-station-session-id ::trap-station-session/trap-station-session-id)
(s/def ::camera-id int?)
(s/def ::trap-station-session-created ::ts/date-time)
(s/def ::trap-station-session-updated ::ts/date-time)
(s/def ::trap-station-session-camera-media-unrecoverable boolean?)
(s/def ::trap-station-session-camera-import-path (s/nilable string?))

(s/def ::trap-station-session-camera
  (s/keys :req-un [::trap-station-session-camera-id
                   ::trap-station-session-id
                   ::camera-id
                   ::trap-station-session-camera-created
                   ::trap-station-session-camera-updated
                   ::trap-station-session-camera-media-unrecoverable]
          :opt-un [::trap-station-session-camera-import-path]))

(s/def ::ptrap-station-session
  (s/keys :opt-un [::trap-station-session-id
                   ::camera-id
                   ::trap-station-session-camera-media-unrecoverable
                   ::trap-station-session-camera-import-path]))

(s/def ::ttrap-station-session
  (s/keys :req-un [::trap-station-session-id
                   ::camera-id]
          :opt-un [::trap-station-session-camera-media-unrecoverable
                   ::trap-station-session-camera-import-path]))
