(ns camelot.spec.model.camera
  (:require [clojure.spec.alpha :as s]
            [clj-time.spec :as ts]))

(s/def ::camera-id int?)
(s/def ::camera-created ::ts/date-time)
(s/def ::camera-updated ::ts/date-time)
(s/def ::camera-name string?)
(s/def ::camera-make (s/nilable string?))
(s/def ::camera-model (s/nilable string?))
;; TODO model camera-status-id
(s/def ::camera-notes (s/nilable string?))

(s/def ::camera
  (s/keys :req-un [::camera-id
                   ::camera-created
                   ::camera-updated
                   ::camera-name]
          :opt-un [::camera-notes
                   ::camera-make
                   ::camera-model]))
