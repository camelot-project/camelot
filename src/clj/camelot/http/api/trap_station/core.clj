(ns camelot.http.api.trap-station.core
  (:require
   [camelot.http.api.trap-station.spec :as spec]
   [camelot.http.api.trap-station.resources :as resources]
   [camelot.http.api.spec.core :as api-core]
   [clojure.edn :as edn]
   [compojure.api.sweet :refer [context DELETE GET POST PATCH]]
   [clojure.spec.alpha :as s]))

(def base-routes
  (context "/trap-stations" {state :state}
    :tags ["trap-stations"]

    (GET "/:id" [id]
      :summary "Retrieve a trap station with the given ID"
      :return (s/merge
               ::api-core/json-api-without-data
               (s/keys :req-un [::spec/data]))
      (resources/get-with-id state id))

    (PATCH "/:id" [id]
      :summary "Update a trap station"
      :body [data (s/keys :req-un [:camelot.http.api.trap-station.patch/data])]
      :return (s/merge
               ::api-core/json-api-without-data
               (s/keys :req-un [::spec/data]))
      (resources/patch state id data))

    (POST "/" []
      :summary "Create a new trap station"
      :body [data (s/keys :req-un [:camelot.http.api.trap-station.post/data])]
      :return (s/merge
               ::api-core/json-api-without-data
               (s/keys :req-un [::spec/data]))
      (resources/post state data))

    (DELETE "/:id" [id]
      :summary "Delete a trap station"
      (resources/delete state id))))

(def survey-routes
  (context "/surveys/:survey-id/trap-stations" {state :state}
    :tags ["trap-stations"]
    (GET "/" [survey-id]
      :summary "Retrieve all trap stations"
      :return (s/merge
               ::api-core/json-api-without-data
               (s/keys :req-un [:camelot.http.api.trap-station.get-all/data]))
      (resources/get-all state (edn/read-string survey-id)))))

(def routes
  (context "/" []
    base-routes
    survey-routes))
