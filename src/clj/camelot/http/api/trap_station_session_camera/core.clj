(ns camelot.http.api.trap-station-session-camera.core
  (:require
   [camelot.http.api.trap-station-session-camera.spec :as spec]
   [camelot.http.api.trap-station-session-camera.resources :as resources]
   [camelot.http.api.spec.core :as api-core]
   [clojure.edn :as edn]
   [compojure.api.sweet :refer [context DELETE GET POST PATCH]]
   [clojure.spec.alpha :as s]))

(def base-routes
  (context "/trap-station-session-cameras" {state :state}
    :tags ["trap-station-session-cameras"]

    (GET "/:id" [id]
      :summary "Retrieve a trap station session camera with the given ID"
      :return (s/merge
               ::api-core/json-api-without-data
               (s/keys :req-un [::spec/data]))
      (resources/get-with-id state id))

    (PATCH "/:id" [id]
      :summary "Update a trap station session camera"
      :body [data (s/keys :req-un [:camelot.http.api.trap-station-session-camera.patch/data])]
      :return (s/merge
               ::api-core/json-api-without-data
               (s/keys :req-un [::spec/data]))
      (resources/patch state id data))

    (POST "/" []
      :summary "Create a new trap station session camera"
      :body [data (s/keys :req-un [:camelot.http.api.trap-station-session-camera.post/data])]
      :return (s/merge
               ::api-core/json-api-without-data
               (s/keys :req-un [::spec/data]))
      (resources/post state data))

    (DELETE "/:id" [id]
      :summary "Delete a trap station session camera"
      (resources/delete state id))))

(def survey-routes
  (context "/trap-station-sessions/:trap-station-session-id/trap-station-session-cameras" {state :state}
    :tags ["trap-station-session-cameras"]
    (GET "/" [trap-station-session-id]
      :summary "Retrieve all trap stations session cameras for a trap station session"
      :return (s/merge
               ::api-core/json-api-without-data
               (s/keys :req-un [:camelot.http.api.trap-station-session-camera.get-all/data]))
      (resources/get-all state (edn/read-string trap-station-session-id)))))

(def routes
  (context "/" []
    base-routes
    survey-routes))
