(ns camelot.http.api.trap-station-session.core
  (:require
   [camelot.http.api.trap-station-session.spec :as spec]
   [camelot.http.api.trap-station-session.resources :as resources]
   [camelot.http.api.spec.core :as api-core]
   [clojure.edn :as edn]
   [compojure.api.sweet :refer [context DELETE GET POST PATCH]]
   [clojure.spec.alpha :as s]))

(def base-routes
  (context "/trap-station-sessions" {state :state}
    :tags ["trap-station-sessions"]

    (GET "/:id" [id]
      :summary "Retrieve a trap station session with the given ID"
      :return (s/merge
               ::api-core/json-api-without-data
               (s/keys :req-un [::spec/data]))
      (resources/get-with-id state id))

    (PATCH "/:id" [id]
      :summary "Update a trap station session"
      :body [data (s/keys :req-un [:camelot.http.api.trap-station-session.patch/data])]
      :return (s/merge
               ::api-core/json-api-without-data
               (s/keys :req-un [::spec/data]))
      (resources/patch state id data))

    (POST "/" []
      :summary "Create a new trap station session"
      :body [data (s/keys :req-un [:camelot.http.api.trap-station-session.post/data])]
      :return (s/merge
               ::api-core/json-api-without-data
               (s/keys :req-un [::spec/data]))
      (resources/post state data))

    (DELETE "/:id" [id]
      :summary "Delete a trap station session"
      (resources/delete state id))))

(def survey-routes
  (context "/trap-stations/:trap-station-id/trap-station-sessions" {state :state}
    :tags ["trap-station-sessions"]
    (GET "/" [trap-station-id]
      :summary "Retrieve all trap stations sessions for a trap station"
      :return (s/merge
               ::api-core/json-api-without-data
               (s/keys :req-un [:camelot.http.api.trap-station-session.get-all/data]))
      (resources/get-all state (edn/read-string trap-station-id)))))

(def routes
  (context "/" []
    base-routes
    survey-routes))
