(ns camelot.http.api.trap-station-session.spec
  (:require
   [camelot.spec.model.trap-station-session :as model-spec]
   [camelot.http.api.spec.core :as api-core]
   [clojure.spec.alpha :as s]))

(s/def ::id ::model-spec/trap-station-session-id)
(s/def ::trapStationId ::model-spec/trap-station-id)
(s/def ::created nat-int?)
(s/def ::updated nat-int?)
(s/def ::startDate nat-int?)
(s/def ::endDate (s/nilable nat-int?))
(s/def ::notes ::model-spec/trap-station-session-notes)
(s/def ::label ::model-spec/trap-station-session-label)

(s/def ::attributes
  (s/keys :req-un [::id
                   ::trapStationId
                   ::created
                   ::updated
                   ::startDate]
          :opt-un [::notes
                   ::label
                   ::endDate]))

(s/def ::data
  (s/keys :req-un [::api-core/id
                   ::api-core/type
                   ::attributes]))

(s/def :camelot.http.api.trap-station-session.patch/attributes
  (s/keys :opt-un [::trapStationId
                   ::startDate
                   ::endDate
                   ::notes]))

(s/def :camelot.http.api.trap-station-session.patch/data
  (s/keys :req-un [::api-core/id
                   ::api-core/type
                   :camelot.http.api.trap-station-session.patch/attributes]))

(s/def :camelot.http.api.trap-station-session.post/attributes
  (s/keys :req-un [::trapStationId
                   ::startDate]
          :opt-un [::endDate
                   ::notes]))

(s/def :camelot.http.api.trap-station-session.post/data
  (s/keys :req-un [::api-core/type
                   :camelot.http.api.trap-station-session.post/attributes]))

(s/def :camelot.http.api.trap-station-session.get-all/data
  (s/coll-of ::data))
