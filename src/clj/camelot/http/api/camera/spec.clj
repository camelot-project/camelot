(ns camelot.http.api.camera.spec
  (:require
   [camelot.spec.model.camera :as model-spec]
   [camelot.http.api.spec.core :as api-core]
   [clojure.spec.alpha :as s]))

(s/def ::id ::model-spec/camera-id)
(s/def ::created nat-int?)
(s/def ::updated nat-int?)
(s/def ::name ::model-spec/camera-name)
(s/def ::make ::model-spec/camera-make)
(s/def ::model ::model-spec/camera-model)
;; TODO model camera-status-id
(s/def ::notes ::model-spec/camera-notes)

(s/def ::attributes
  (s/keys :req-un [::name
                   ::make
                   ::model]
          :opt-un [::notes]))

(s/def ::data
  (s/keys :req-un [::api-core/id
                   ::api-core/type
                   ::attributes]))

(s/def :camelot.http.api.camera.patch/attributes
  (s/keys :opt-un [::name
                   ::make
                   ::model
                   ::notes]))

(s/def :camelot.http.api.camera.patch/data
  (s/keys :req-un [::api-core/id
                   ::api-core/type
                   :camelot.http.api.camera.patch/attributes]))

(s/def :camelot.http.api.camera.post/attributes
  (s/keys :req-un [::name]
          :opt-un [::make
                   ::model
                   ::notes]))

(s/def :camelot.http.api.camera.post/data
  (s/keys :req-un [::api-core/type
                   :camelot.http.api.camera.post/attributes]))

(s/def :camelot.http.api.camera.get-all/data
  (s/coll-of ::data))
