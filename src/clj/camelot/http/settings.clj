(ns camelot.http.settings
  (:require [camelot.translation.languages :as languages]
            [camelot.util.cursorise :as cursorise]
            [clojure.tools.logging :as log]
            [compojure.core :refer [context GET POST]]
            [ring.util.response :as r]))

(defn- resolve-language
  [language]
  (if (languages/languages language)
    language
    (do
      (log/warn "Received invalid language:" language)
      :en)))

(defn- select-language
  "Set cookies for the given language."
  [language]
  {:status 204
   :session {:language (resolve-language language)}
   :body nil})

(def routes
  (context "/settings" {state :state}
           (GET "/" [] (-> (get state :config)
                           (update :detector dissoc :username :password)
                           cursorise/cursorise
                           r/response))
           (POST "/language/:id" [id]
                 (select-language (keyword id)))))
